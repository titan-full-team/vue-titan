/**
 * @param formRefs
 * @returns {boolean}
 */
export function validateMultipleForms(formRefs = []) {
    let valid = true;
    formRefs.forEach(item => {
        if (item.isValid() === false) {
            valid = false;
        }
    });
    return valid;
}

/**
 * @param num
 * @returns {string}
 */
export function prettyBytes(num) {
    // jacked from: https://github.com/sindresorhus/pretty-bytes
    if (typeof num !== 'number' || isNaN(num)) {
        throw new TypeError('Expected a number');
    }

    let exponent;
    let unit;
    let neg = num < 0;
    const units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    if (neg) {
        num = -num;
    }

    if (num < 1) {
        return (neg ? '-' : '') + num + ' B';
    }

    exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
    num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
    unit = units[exponent];

    return (neg ? '-' : '') + num + ' ' + unit;
}
