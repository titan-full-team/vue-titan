import axios from 'axios'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import moment from 'moment'

Vue.use(VueI18n);

export class TitanTranslator {

    i18n = null;
    defaultLanguage = 'gb';
    loadMessageFromApi = false;
    apiBasePath = '';
    type = 'ADM';
    loaded = false;

    localStorageKeys = 'titan-default';
    localStorageLang = 'titan-lang-default';

    constructor(apiBasePath, translatorEnv) {
        this.apiBasePath = apiBasePath;
        this.type = translatorEnv.app;
        this.localStorageKeys = 'titan-' + this.type;
        this.localStorageLang = 'titan-lang-' + this.type;
        this.defaultLanguage = translatorEnv.supportedLanguages[0].key;
        this.loadMessageFromApi = translatorEnv.loadFromApi;
        this.init();
    }

    getDefaultLanguage() {
        const lang = localStorage.getItem(this.localStorageLang);
        return lang === null ? this.defaultLanguage : lang;
    }

    init() {
        if (this.i18n === null) {
            const lang = this.getDefaultLanguage();
            this.i18n = new VueI18n({
                locale: lang,
                fallbackLocale: lang,
                missing: (locale, key, translator) => {
                    if (this.loaded === true) {
                        this.storeString(key);
                    }
                }
            });
            Vue.mixin(translatorMixin(this));
        }
    }

    storeString(key) {
        let strings = this.getStoredStrings();

        if (strings.indexOf(key) === -1) {
            strings.push(key);
            localStorage.setItem(this.localStorageKeys, JSON.stringify(strings));
        }
    }

    areStoredStrings() {
        return this.getStoredStrings().length !== 0;
    }

    getStoredStrings() {
        const data = localStorage.getItem(this.localStorageKeys);
        return data === null ? [] : JSON.parse(data);
    }

    pushStrings() {
        const slugs = this.getStoredStrings();
        return axios.post(this.apiBasePath + 'translate/' + this.type, {slugs});
    }

    removeStoredStrings() {
        localStorage.removeItem(this.localStorageKeys);
    }

    loadMessages(lang) {
        return new Promise((resolve, reject) => {
            this.loaded = false;
            this.getMessages(lang).then(msgs => {
                this.setLanguageMessages(lang, msgs);
                this.loaded = true;
                resolve(lang);
            }).catch(error => {
                reject(error);
            });
        });
    }

    getMessages(lang) {
        if (this.loadMessageFromApi === true) {
            return new Promise((resolve, reject) => {
                axios.get(this.apiBasePath + 'translate/translates/' + lang + '/' + this.type).then(res => {
                    resolve(res.data);
                }).catch(error => {
                    reject(error);
                })
            });
        }
        return import('@/locales/' + lang + '.json');
    }

    setLanguageMessages(lang, messages) {
        localStorage.setItem(this.localStorageLang, lang);
        this.i18n.setLocaleMessage(lang, messages);
        this.i18n.locale = lang;
        axios.defaults.headers.common['Accept-Language'] = lang;
        document.querySelector('html').setAttribute('lang', lang);
    }

    changeLanguage(lang) {
        if (this.i18n.locale !== lang) {
            return this.loadMessages(lang);
        }
        return Promise.resolve(lang);
    }

    formatDate(date) {
        return moment(date).format('DD.MM.YYYY');
    }

    formatDateTime(date) {
        return moment(date).format('DD.MM.YYYY HH:mm:ss');
    }

    formatNumber(number, minimumFractionDigits = 2) {
        return Number(number).toLocaleString("sk-SK", {minimumFractionDigits: minimumFractionDigits});
    }

    formatTime(date) {
        return moment(date).format('HH:mm:ss');
    }

    tc(string, number) {
        return this.i18n === null ? string : this.i18n.tc(string, number === undefined ? 999 : number);
    }

    t(string) {
        return this.i18n === null ? string : this.i18n.t(string, 1);
    }
}

export function translatorMixin(translator) {
    Vue.filter('time', (date) => {
        return translator.formatTime(date);
    });
    Vue.filter('date', (date) => {
        return translator.formatDate(date);
    });
    Vue.filter('dateTime', (date) => {
        return translator.formatDateTime(date);
    });
    Vue.filter('number', (number, minimumFractionDigits) => {
        return translator.formatNumber(number, minimumFractionDigits);
    });
    Vue.filter('tc', (string, number) => {
        return translator.tc(string, number);
    });
    Vue.filter('t', (string) => {
        return translator.t(string, string);
    });

    return {
        methods: {
            tc(string, number) {
                return translator.tc(string, number);
            },
            t(string) {
                return translator.t(string);
            },
            formatTime(date) {
                return translator.formatTime(date);
            },
            formatDate(date) {
                return translator.formatDate(date);
            },
            formatDateTime(date) {
                return translator.formatDateTime(date);
            },
            formatNumber(number) {
                return translator.formatNumber(number);
            }
        },
        data() {
            return {
                i18n: translator,
                supportedLangs: process.env.translator.supportedLanguages
            }
        }
    }
}
