export const getSortObject = function (sortable, columns) {
    const setting = getSettingByKey(columns, sortable.key);
    let out = {};
    out[sortable.key] = sortable.direction;

    // Apply sortField setting
    if (setting !== undefined && setting.sortField !== undefined) {
        if (setting.sortField instanceof Array) {
            out = {};
            setting.sortField.forEach(sortColumn => {
                out[sortColumn] = sortable.direction;
            });
        } else if (typeof setting.sortField === 'string') {
            out = {};
            out[setting.sortField] = sortable.direction;
        }
    }

    return out;
};

export const getSettingByKey = function(columns, key) {
    return columns.find(res => {
        return key === res.key;
    });
};

export const getConditionsObject = function (conditions, columns) {
    let out = [];

    Object.keys(conditions).forEach(columnKey => {
        const setting = getSettingByKey(columns, columnKey);

        if (setting !== undefined && setting.filter !== undefined && setting.filter.field !== undefined) {
            let condition = {...conditions[columnKey]};
            condition.key = setting.filter.field;
            condition.field = undefined;
            out = formatConditionValue(out, condition);
        } else {
            let condition = conditions[columnKey];
            condition.key = columnKey;
            out = formatConditionValue(out, condition);
        }

    });

    return out;
};

export const formatConditionValue = function(out, condition) {
    if (typeof condition.value === 'object' && condition.value.from !== undefined) {
        if (condition.value.from !== null) {
            const fromCondition = {...condition}
            fromCondition.value = condition.value.from;
            fromCondition.operator = '>=';
            out.push(fromCondition);
        }
        if (condition.value.to !== null) {
            const toCondition = {...condition}
            toCondition.value = condition.value.to;
            toCondition.operator = '<=';
            out.push(toCondition);
        }
    } else {
        out.push(condition);
    }
    return out;
}

