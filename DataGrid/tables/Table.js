import Column from '../Column'
import AcceptAction from './../../AcceptAction'
import Buttons from "../../Buttons";
import GridFilter from "../GridFilter";

import {library} from '@fortawesome/fontawesome-svg-core'
import {faSort, faCog, faSortUp, faSortDown} from '@fortawesome/free-solid-svg-icons'

library.add(faSort, faCog, faSortUp, faSortDown);

export const table = {
    components: {
        GridFilter,
        Buttons,
        Column,
        AcceptAction
    },
    props: {
        settings: {
            type: Object,
            required: true
        },
        conditions: {
            type: Object,
            required: true
        },
        sortable: {
            type: Object,
            required: true
        },
        items: {
            type: Array,
            required: true
        },
        loading: {
            type: Boolean,
            required: false,
            default: false,
        }
    },
    computed: {
        isSetFilter() {
            return this.columns.find(item => {
                return item.filter !== undefined;
            }) !== undefined;
        },
        columns() {
            if (this.settings.columns === undefined) {
                return [];
            }
            return this.settings.columns.filter(item => {
                const visible = item.visible === undefined ? true : item.visible;
                return typeof visible === 'function' ? visible() : visible;
            });
        },
        buttons() {
            return this.settings.buttons === undefined ? [] : this.settings.buttons;
        }
    },
    methods: {
        getRowClass(item) {
            const rowClass = this.settings.rowClass === undefined ? true : this.settings.rowClass;
            return typeof rowClass === 'function' ? rowClass(item) : rowClass;
        },
        setConditions(column, value) {
            const conditions = {...this.conditions};
            const key = this.getFilterKey(column);
            conditions[key] = column.filter;
            conditions[key].value = value;
            this.$emit('onFilter', conditions);
        },
        resetFilter(column) {
            const conditions = {...this.conditions};
            const key = this.getFilterKey(column);
            delete conditions[key];
            this.$emit('onFilter', conditions);
        },
        getSortIcon(column) {
            if (column.key !== this.sortable.key) {
                return 'sort';
            } else if (column.key === this.sortable.key && this.sortable.direction === 'DESC') {
                return 'sort-down';
            }
            return 'sort-up';
        },
        isSortable(column) {
            return column.sortable === true || column.sortable === undefined;
        },
        getFilterKey(column) {
            return column.filter.key === undefined ? column.key : column.filter.key;
        },
        setSortable(column) {
            if (this.isSortable(column) === true) {
                const key = column.key;
                const defaultDirection = column.defaultSort === undefined ? 'ASC' : column.defaultSort;

                if (this.sortable.key === key) {
                    this.sortable.direction = this.sortable.direction === 'DESC' ? 'ASC' : 'DESC';
                } else {
                    this.sortable.direction = defaultDirection;
                }
                this.sortable.key = key;
            }
        },
    },
    data() {
        return {};
    }
};
